<?php
    class Model_Auth extends Model
    {
        public $dbh;
        public function check_data($email)
        {
            $stmt   = $this->dbh->prepare("SELECT * FROM `users` WHERE `users`.`email` = ?;");
            mysqli_stmt_bind_param($stmt, 's', $email);
            $stmt->execute();
            $res    = $stmt->get_result();
            $result = $res->fetch_all();
            return $result;
            
        }
    }
?>