<?php
class Model_Tags extends Model
{
    public $dbh;
    //получаем список тегов
    public function get_data()
    {
        $sth = $this->dbh->prepare('SELECT id, title
                FROM tags ');
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        return $result;
    }
    //добавляем теги
    public function add_data($title)
    {
        $stmt = $this->dbh->prepare("INSERT INTO `tags` (`id`, `title`) VALUES (NULL, ?);");
        mysqli_stmt_bind_param($stmt, 's', $title);

        $stmt->execute();
    }
    //удаляем теги
    public function del_data($id)
    {
        $stmt = $this->dbh->prepare("DELETE FROM `tags` WHERE `tags`.`id` = ?;");
        mysqli_stmt_bind_param($stmt, 's', $id);

        $stmt->execute();
    }
    //обновляем теги
    public function update_data($title, $id)
    {
        $stmt = $this->dbh->prepare("UPDATE `tags` SET `title` = ? WHERE `tags`.`id` = ?;");
        mysqli_stmt_bind_param($stmt, 'ss', $title, $id);

        $stmt->execute();
    }
}
?>