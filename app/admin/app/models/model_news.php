<?php
    class Model_News extends Model
    {
        public $dbh;
        //тестовые данные
        public function get_data2()
        {	
            return array(
			
			array(
            'Title'       => '2012',
            //'Site'      => 'http://DunkelBeer.ru',
            'Description' => 'Промо-сайт темного пива Dunkel от немецкого производителя Löwenbraü выпускаемого в России пивоваренной компанией "CАН ИнБев".'
			),
			array(
            'Title'       => '2012',
            //'Site'      => 'http://ZopoMobile.ru',
            'Description' => 'Русскоязычный каталог китайских телефонов компании Zopo на базе Android OS и аксессуаров к ним.'
			),
			// todo
            );
        }
        //информация о новостях
        public function get_data()
        {
            $sth          = $this->dbh->prepare('SELECT n.id, n.title, n.description,  GROUP_CONCAT(t.title SEPARATOR ", ") AS tags  FROM `newslist` as n Left OUTER JOIN `tags_news` as tn ON n.id = tn.id_of_news Left OUTER  JOIN `tags` as t ON tn.id_of_tag = t.id GROUP BY n.id ORDER BY  n.id DESC;');
            $sth->execute();
            $res          = $sth->get_result();
            $result       = $res->fetch_all();
            return $result;
        }
        //информация о новости
        public function get_dataid($id)
        {
            $query = trim($id); 
            $query = htmlspecialchars($id);
            $sth          = $this->dbh->prepare('SELECT n.id, n.title, n.description,  
            GROUP_CONCAT(t.title SEPARATOR ", ") AS tags  FROM `newslist` as n 
            Left OUTER JOIN `tags_news` as tn ON n.id = tn.id_of_news 
            Left OUTER  JOIN `tags` as t ON tn.id_of_tag = t.id 
            where n.id=? GROUP BY n.id ORDER BY  n.id DESC ;');
            mysqli_stmt_bind_param($sth, 's',$id);
            $sth->execute();
            $res          = $sth->get_result();
            $result       = $res->fetch_all();
            return $result;
        }
                //информация о новостях
        public function get_datasearch($query)
        {
            $query = trim($query); 
            $query = htmlspecialchars($query);
            $query = "%".$query."%";
            $sth          = $this->dbh->prepare('SELECT id, title, Description
            FROM newslist where title LIKE ? or description LIKE ?');
            mysqli_stmt_bind_param($sth, 'ss',$query, $query);
            $sth->execute();
            $res          = $sth->get_result();
            $result       = $res->fetch_all();
            return $result;
        }
        //добавляем носвость
        public function add_data($title, $description)
        {
            $stmt         = $this->dbh->prepare("INSERT INTO `newslist` (`id`, `title`, `description`) VALUES (NULL, ?,?);");
            mysqli_stmt_bind_param($stmt, 'ss', $title, $description);
            
            $stmt->execute();
            return $stmt->insert_id;
        }
        //удаляем новость
        public function del_data($id)
        {
            $stmt         = $this->dbh->prepare("DELETE FROM `newslist` WHERE `newslist`.`id` = ?;");
            mysqli_stmt_bind_param($stmt, 's', $id);
            
            $stmt->execute();
            $this->del_tags_from_news($id);
        }
        //обновляем новость
        public function update_data($title, $description, $id)
        {
            $stmt         = $this->dbh->prepare("UPDATE `newslist` SET `title` = ?,`description` = ? WHERE `newslist`.`id` = ?;");
            mysqli_stmt_bind_param($stmt, 'sss', $title, $description, $id);
            
            $stmt->execute();
            $this->del_tags_from_news($id);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////связанная таблица    
        //добавим теги новости
        public function add_tags_to_news($idnews, $idtag)
        {
            $stmt         = $this->dbh->prepare("INSERT INTO `tags_news` (`id`, `id_of_news`, `id_of_tag`) VALUES (NULL, ?,?);");
            mysqli_stmt_bind_param($stmt, 'ss', $idnews, $idtag);
            
            $stmt->execute();
        }
        //удалим теги из новости
        public function del_tags_from_news($idnews)
        {
            $stmt         = $this->dbh->prepare("DELETE FROM `tags_news` WHERE `tags_news`.`id_of_news` = ?;");
            mysqli_stmt_bind_param($stmt, 's', $idnews);
            
            $stmt->execute();
        }
    }
?>