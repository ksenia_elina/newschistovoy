
<form method="post" >
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Enter title" value="<?= $data[0][1]?>">
  </div>
  <div class="form-group">
    <label for="Description">Description</label>
    <input type="text" class="form-control" id="Description" name="Description" placeholder="Description" value="<?= $data[0][2]?>">
  </div>
  
    <div class="form-group">
    <label for="id_of_tag">Tags</label>
    <input type="text" class="form-control" id="id_of_tag" name="id_of_tag" placeholder="tags with ," value="<?= $data[0][3]?>">
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>