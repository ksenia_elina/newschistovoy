<?php
    class Controller_News extends Controller
    {
        
        function __construct()
        {
            $this->model          = new Model_News();
            $this->view           = new View();
        }
        //выводим список
        function action_index()
        {
            If(!(Isset($_GET['search'])))
            {
                $data                 = $this->model->get_data();		
            }else
            {
                $query = $_GET['search'];
                if (!empty($query)) 
                { 
                    if (strlen($query) < 3) {
                        $text = '<p>Слишком короткий поисковый запрос.</p>';
                    } else if (strlen($query) > 128) {
                        $text = '<p>Слишком длинный поисковый запрос.</p>';
                    } else{ 
                        $data                 = $this->model->get_datasearch($query);
                    }
                } else {
                    $text = '<p>Задан пустой поисковый запрос.</p>';
                }
            }
            
            $this->view->generate('news_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest(),$text);
        }
        //добавляем новость
        function action_add($Title = NULL, $Description=NULL)
        {
            //делим теги по запятым
            if (isset($_POST['title']) && isset($_POST['Description']))
            {
                $tags             = explode(',', $_POST['id_of_tag']);
                $data             = $this->model->add_data($_POST['title'],$_POST['Description']);
                //////////////////////////////////////////////////////////////////////////////////////
                foreach($tags as &$value)
                {
                    $this->model->add_tags_to_news($data,$value);
                }
                /////////////////////////////////////////////////////////////////////////////////////
                $host             = 'http://'.$_SERVER['HTTP_HOST'].'/admin/news/';
                header('Location:'.$host);
            }
            else
            {
                $this->view->generate('newsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
            }
        }
        
        function action_del()
        {
            $data                 = $this->model->del_data($_GET['id']);		
            //удаление тегов
            $host                 = 'http://'.$_SERVER['HTTP_HOST'].'/admin/news/';
            header('Location:'.$host);
            
        }
        //обновляем новости
        function action_update()
        {
            if (isset($_POST['title']) && isset($_POST['Description']))
            {
                $data             = $this->model->update_data($_POST['title'],$_POST['Description'], $_GET['id']);		
                $tags             = explode(',', $_POST['id_of_tag']);
                //////////////////////////////////////////////////////////////////////////////////////
                foreach($tags as &$value)
                {
                    $this->model->add_tags_to_news($_GET['id'],$value);
                }
                /////////////////////////////////////////////////////////////////////////////////////
                $host             = 'http://'.$_SERVER['HTTP_HOST'].'/admin/news/';
                header('Location:'.$host);
            }
            else
            {
                $data             = $this->model->get_dataid($_GET['id']);
                $this->view->generate('newsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
            }
        }
        //проверка на юзера
        public static function isGuest()
        {
            if (isset($_SESSION['user']))
                return false;
            else 
                return true;
        }
    }
?>