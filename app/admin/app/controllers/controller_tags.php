<?php
    class Controller_tags extends Controller
    {
        
        function __construct()
        {
            $this->model           = new Model_Tags();
            $this->view            = new View();
        }
        //список тегов
        function action_index()
        {
            $data                  = $this->model->get_data();		
            $this->view->generate('tags_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
        }
        //добавляем тег
        function action_add($Title = NULL, $Description=NULL)
        {
            //добавляем и переходим на индекс
            if (isset($_POST['title']))
            {
                $data              = $this->model->add_data($_POST['title']);		
                $host              = 'http://'.$_SERVER['HTTP_HOST'].'/admin/tags/';
                header('Location:'.$host);
            }
            else
            {
                $this->view->generate('tagsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
            }
        }
        //удаляем тег
        function action_del()
        {
            $data                  = $this->model->del_data($_GET['id']);		
            $host                  = 'http://'.$_SERVER['HTTP_HOST'].'/admin/tags/';
            header('Location:'.$host);
            
        }
        //обновляем теги
        function action_update()
        {
            if (isset($_POST['title']))
            {
                $data                      = $this->model->update_data($_POST['title'], $_GET['id']);		
                $host                      = 'http://'.$_SERVER['HTTP_HOST'].'/admin/tags/';
                header('Location:'.$host);
            }
            else
            {
                $this->view->generate('tagsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
            }
        }
        }
        ?>        