<?php
class Controller_Auth extends Controller
{

	function __construct()
	{
		$this->model = new Model_Auth();
		$this->view = new View();
	}
	
    //определяем есть ли пользователь
	function action_index()
	{
        $email = false;
        $password = false;
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $errors = false;
            $check = $this->model->check_data($_POST['email']);
            $hashed_password = $check[0][2];
            $userId = $check[0][0];
            var_dump($this->verify($password, $hashed_password));
            if ($this->verify($password, $hashed_password)) {// если есть, то одна вью, если нет, то другая
                $this->auth($userId);
                $this->view->generate('authmain_view.php', 'template_view.php', $data,'app/admin/');
            } else 
            {
                $this->view->generate('auth_view.php', 'template_view.php', $data,'app/admin/');
            }
            
        }
        else
        {
            $this->view->generate('auth_view.php', 'template_view.php', $data,'app/admin/');
        }
	}
    //будет реализация регистрации
   function action_login()
   {
       
   }
   //окончание сеанса
   function action_logout()
   {
        unset($_SESSION["user"]);
        //session_start();
        header("Location: /");
        return true;
   }
	//проверка пароля
    function verify($password, $hashedPassword) {
        return hash_equals($hashedPassword, crypt($password, $hashedPassword));
    }
    //точно есть пользователь
    public static function auth($userId)
    {
        // Записываем идентификатор пользователя в сессию
        $_SESSION['user'] = $userId;
    }
    //Функция для определения гость ли пользователь
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) return false;
        else return true;
    }
}
?>