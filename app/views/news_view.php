
<h1>Новости</h1>
<p>
<table>
Лента новостей.
<tr><td>Title</td><td>Description</td><td>Tags</td></tr>
<?php
        $host = 'http://'.$_SERVER['HTTP_HOST'];
        //var_dump(count($data));
        // Переменная хранит число сообщений выводимых на станице
        $num = 5;
        // Извлекаем из URL текущую страницу
        $page = $_GET['page'];
        $posts = count($data);
        // Определяем начало сообщений для текущей страницы
        $page = intval($page);
        // Находим общее число страниц
        $total = intval(($posts - 1) / $num) + 1;
        // Если значение $page меньше единицы или отрицательно
        // переходим на первую страницу
        // А если слишком большое, то переходим на последнюю
        if(empty($page) or $page < 0) $page = 1;
          if($page > $total) $page = $total;
        // Вычисляем начиная к какого номера
        // следует выводить сообщения
        $start = $page * $num - $num;
        $submass = array_slice($data, $start, $num);
        
        foreach($submass as $row)
        {
            $tags = explode(',', $row[3]);
            echo '<tr><td>'.$row[1].'</td><td>'.$row[2].'</td><td>';
            foreach($tags as $tag)
            {
                echo '<a href="'.$host."/news?tag=".$tag.'">'.$tag.'</a>';
            }
            echo '</td></tr>';
        }

?>
</table>
</p>
<?php
    for($i=1; $i < $total+1; $i++)
    {
        echo '<a href="'.$host.'?page='.$i.'">'.$i.'</a>';
    }
    
?>